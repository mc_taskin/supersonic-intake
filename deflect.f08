subroutine deflect(mach,gam,def_max,shock_max)
    implicit none
    real,intent(in) :: mach,gam
    real,intent(out) :: def_max,shock_max
    real :: A,B
    
    A = ((1/(gam*mach**2))*((gam+1)/4*mach**2-1+sqrt((gam+1)*((gam+1)/16*mach**4+(gam-1)/2*mach**2+1))))**0.5
    shock_max = asin(A)
    B = 1/(tan(shock_max)*((gam+1)/2*mach**2/(mach**2*sin(shock_max)**2-1)-1))
    def_max = atan(B)
  end subroutine deflect
