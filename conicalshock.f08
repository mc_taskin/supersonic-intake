module conicalshock

contains
  subroutine downstream (mach1,gam,shock_arc,mach2,V_2_nondim,def_shock)
    real, intent(in) :: mach1,gam,shock_arc
    real, intent(out):: mach2,V_2_nondim,def_shock
    real :: A,B,C,D
    A = 1+(gam-1)/2*mach1**2
    B = gam*mach1**2*sin(shock_arc)**2-(gam-1)/2
    C = mach1**2*cos(shock_arc)**2
    D = 1+(gam-1)/2*mach1**2*sin(shock_arc)**2

    mach2 = (A/B + C/D)**0.5

    A = (gam-1)*mach2**2
    
    V_2_nondim = 1/sqrt(1+2/A)

    A = tan(shock_arc)**(-1)
    B = mach1**2*sin(shock_arc)**2-1
    C = (gam+1)/2*mach1**2-(mach1**2*sin(shock_arc)**2-1)
    D = A*B/C
    def_shock = atan(D)
  end subroutine downstream

subroutine modeuler(V_2_nondim,def_shock,shock_angle,gam,dtet,no,ray_angle,v_r_f,v_t_f,V,Mach,local_def,cone_angle)
    real, intent(in) :: gam
    real,intent(inout) :: dtet,V_2_nondim,shock_angle,def_shock
    real,intent(out) :: ray_angle,v_r_f,v_t_f,V,Mach,local_def 
    real :: A,B,C,v_r_o,v_t_o,v_r_p,v_t_p,ray_angle_1
    integer,intent(in) :: no
    integer :: i
    real :: pi = 3.14159
    character(len=70) :: fn
30  format('Number',T13,'Teta',T24,'V_r',T35,'V_t',T46,'V',T57,'Mach',T68,'Deflect'  / &
            6('-'),T11,8('-'),T23,7('-'),T34,7('-'),T45,7('-'),T56,7('-'),T67,7('-') /)
40 format (I6,T12,F7.4,T24,F7.4,T35,F7.4,T46,F7.4,T57,F7.4,T68,F7.4)

    dtet = DTR(dtet)

    v_r_o = V_2_nondim*cos(shock_angle-def_shock)
    v_t_o = -V_2_nondim*sin(shock_angle-def_shock)

    write(fn,fmt='(i0,a)') no, '.txt'
    open(unit=no, file = fn, form = 'formatted') 
    write(no,30)
    do 1000 i = 0,100000
       ray_angle = shock_angle-(i*dtet)
       if (i == 0) then
          v_r_f= v_r_o
          v_t_f = v_t_o
          V = (v_r_f**2+v_t_f**2)**0.5
          A = 2*V**2
          B = gam-1
          C = (1-V**2)
       
       Mach = (A/(B*C))**0.5
       local_def = atan(v_t_f/v_r_f)
       ray_angle = RTD(ray_angle)
    else if (i >  0) then
       
       v_r_p = v_r_o - v_t_o*dtet
       v_t_p = v_t_o- FRT(gam,v_r_o,v_t_o,ray_angle)*dtet

       v_r_f= v_r_o - (v_t_p + v_t_o)/2*dtet
       v_t_f= v_t_o - (FRT(gam,v_r_o,v_t_o,ray_angle)+ FRT(gam,v_r_p,v_t_p,ray_angle))/2*dtet

       V = (v_r_f**2+v_t_f**2)**0.5
       A = 2*V**2
       B = gam-1
       C = (1-V**2)

       Mach = (A/(B*C))**0.5
       local_def = atan(v_t_f/v_r_f)
       ray_angle = RTD(ray_angle)
    endif
    
    
    write(no, 40) i,ray_angle,v_r_f,v_t_f,V,Mach,local_def
    
    ray_angle = DTR(ray_angle)

    
    if (local_def .gt. 0) then
       print*, 'The cone angle is reached at',i,'th iteration'
       write(no,*) 'The',i,'th iteration is the cone angle'
       ray_angle_1 = shock_angle-((i-1)*dtet)
       cone_angle = ray_angle + (ray_angle_1-ray_angle)/(v_t_o-v_t_f)*(-v_t_f)
       exit
    endif

    v_t_o = v_t_f
    v_r_o = v_r_f
1000 end do
 dtet= RTD(dtet)
 close(1000)

 
  contains
    function FRT (gam,v_r,v_t,angle)
      real :: FRT
      real, intent(in) ::v_r,v_t,angle,gam
      real :: A,B,C,D,E,F

      A = -v_r
      B = (gam-1)/2
      C = (1-(v_r**2+v_t**2))
      D = (v_r+v_t*tan(angle)**(-1))
      E = v_t**2
      F = 1-(v_r**2+v_t**2)

      FRT = A + B*C*D/(E-B*F)
    end function FRT

    function RTD(x)
      real :: RTD,x
      RTD = x*180/pi
    end function RTD

    function DTR(x)
      real:: DTR,x
      DTR = x*pi/180
    end function DTR
    
  end subroutine modeuler



end module conicalshock
