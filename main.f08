! Muratcan Taşkın MAK 576 Supersonic Intake Design Main Code

program main
  use conicalshock
  implicit none

  ! Interface blocks
  Interface
     subroutine deflect(mach,gam,def_max,shock_max)
       real,intent(in) :: mach,gam
       real,intent(out) :: def_max,shock_max
       real :: A,B
     end subroutine deflect

     subroutine shockangle (mach,def,gam,strong,weak)
       real, intent(in) :: mach,def,gam
       real, intent(out) :: strong,weak
       real :: xnew,xold,A,B,C,error,tet1,tet2,x2
       integer :: i = 0
     end subroutine shockangle

!     subroutine modeuler(V_2_nondim,def_shock,shock_angle,gam,dtet,i,ray_angle,v_r_f,v_t_f,V,Mach,local_def)
!       real, intent(in) :: shock_angle,V_2_nondim,def_shock,gam
!       real,intent(inout) :: dtet
!       integer,intent(in) :: i
!       real,intent(out) :: ray_angle,v_r_f,v_t_f,V,Mach,local_def 
!       real :: A,B,C,v_r_o,v_t_o,v_r_p,v_t_p
!     end subroutine modeuler
  end Interface

    ! Invariants
  real, parameter :: R = 287, pi = 3.14159,gam_air = 1.4
  real :: dteta= 0.1
  ! Fligh specification inputs
    real :: M_cruise
    integer ::  alt

    ! Flight specification outputs
    real :: T_cruise, V_cruise, a_cruise

    ! Geometrical specifications for user input
    real :: vert_angle
    integer :: ext_shocks,int_shocks
    ! First Geometrical shock outputs
    real :: max_shock,max_def
    real :: s_a_shock,w_a_shock
    real :: mach_angle
    real :: M_1,def_a_1,V_1_dimless,V_1_r_dimless,V_1_t_dimless,cone_angle_1
    real :: V_local,Mach_local,def_local

    ! other Shocks
    real :: shock_input, mach_angle_2,mach_shock2
    real :: M_2,def_a_2,V_2_dimless,V_2_r_dimless,v_2_t_dimless,cone_angle_2
    real :: V2_local,Mach2_local,def2_local,mach_wave_angle
    
    ! Miscellaneous variant
    character :: response
    integer :: number=0

    
    ! Print formats
10  format('Mach Number',T16,'Velocity',T29,'Speed of Sound',T48,'Altitude',T61,'Temperature' / &
         11('-'),T16,8('-'),T29,14('-'),T48,8('-'),T61,11('-') /)
20  format(F5.2,T16,F7.2,T29,F6.2,T48,I5,T61 ,F5.1)



! INPUTS AND PREPARATION SECTION OF THE CODE

    ! Flow specification inputs
    print*, 'WARNING:'
    print*, 'The code can calculate conical intake with zero angle of attack'
    print*, 'WARNING: Please input the values kg,m,s,K base'
    print*, 'Please enter the specifications of the flight conditions'
    print*, 'The required specifications are mach number, altitude of the cruise'
    read*,  M_cruise, alt

    if (alt <= 11000) then
    T_cruise =  288.15-(3.2*alt/500)
    else
    T_cruise = 216.7
    endif
    a_cruise = (gam_air*R*T_cruise)**(0.5)
    V_cruise = M_cruise*a_cruise
    print 10
    print 20, M_cruise, V_cruise, a_cruise, alt, T_cruise

    ! Shockwave and deflection angle cross-check
    call deflect(M_cruise,gam_air,max_def,max_shock)
    max_def = RTD(max_def)
    max_shock = RTD(max_shock)
    print*, 'Maximum deflection angle is ', max_def
    print*, 'Maximum shockwave angle for maximum deflection angle is ', max_shock
    print*, 'Please input the geometrical vertex deflection angle'
    read*, vert_angle

    do while (vert_angle .gt. max_def)
        print*, 'Detached shock problem, please make sure input is less than', max_def
        read*, vert_angle
     end do
     vert_angle = DTR(vert_angle)

     call shockangle(M_cruise,vert_angle,gam_air,s_a_shock,w_a_shock)

     w_a_shock = RTD(w_a_shock)
     s_a_shock = RTD(s_a_shock)

     print*, 'The weak shock angle solution is', w_a_shock
     print*, 'The strong shock angle solution is', s_a_shock
     print*, 'Generally, the conical intake shock angle solutions are weak solutions. Hence moving forward with the weak shock'
     print*, 'Would you like to proceed?(Y/N)'
     read*,  response
     w_a_shock = DTR(w_a_shock)
     s_a_shock = DTR(s_a_shock)

     
     if (response == 'N')  stop

     print*, 'Making transition to 3D analysis from 2D'
     
650  print*, 'Would you like to add an external shock? (1:First shock,2:Additional shock,0:no)'
700  read*, ext_shocks 



     select case (ext_shocks)
     case (1)
        number = number + 1
        w_a_shock = RTD(w_a_shock)
        print*, 'For the first shock calculations mach', M_cruise, 'will be used as first guess'
        print*, 'For the first shock calculations shock ', w_a_shock, 'will be used as first guess'
        w_a_shock = DTR(w_a_shock)
        call downstream(M_cruise,gam_air,w_a_shock,M_1,V_1_dimless,def_a_1)
        call modeuler(V_1_dimless,def_a_1,w_a_shock,gam_air,dteta,number,mach_angle,V_1_r_dimless, &
             V_1_t_dimless,V_local,Mach_local,def_local,cone_angle_1)
        print*, 'First shock data is written in a file called',number,'.txt'  
        print*, 'please check the file and use it to input for additional shock'
        cone_angle_1 = RTD(cone_angle_1)
        vert_angle = RTD(vert_angle)
        print*, 'Iterated cone angle is', cone_angle_1, 'The initial guess was', vert_angle
        cone_angle_1 = DTR(cone_angle_1)
        vert_angle = DTR(vert_angle)
        go to 650
     case (2)
        
        number = number + 1
        print*, 'Please enter the Mach number'
        read*, Mach_shock2
        mach_wave_angle = asin(1/Mach_shock2)
        mach_wave_angle = RTD(mach_wave_angle)
        print*, 'Please enter the desired shock angle in degree'
        print*, 'Minimal shockwave angle is', mach_wave_angle
        call deflect(Mach_shock2,gam_air,max_def,max_shock)
        max_def = RTD(max_def)
        max_shock = RTD(max_shock)
        print*, 'Maximum deflection angle is ', max_def
        print*, 'Maximum shockwave angle for maximum deflection angle is ', max_shock
        read*, shock_input
        shock_input = DTR(shock_input)
        call downstream(Mach_shock2,gam_air,shock_input,M_2,V_2_dimless,def_a_2)
        call modeuler(V_2_dimless,def_a_2,shock_input,gam_air,dteta,number,mach_angle_2,V_2_r_dimless, &
             V_2_t_dimless,V2_local,Mach2_local,def2_local,cone_angle_2)
        print*, 'The shock data is written in a file called',number,'.txt'  
        print*, 'please check the file and use it to input for additional shock'
        cone_angle_2 = RTD(cone_angle_2)
        print*, 'Iterated cone angle is', cone_angle_2
        cone_angle_2 = DTR(cone_angle_2)
        go to 650
     case (0)
        print*, 'Advancing to internal shocks'
     case default
        print*, 'Invalid Input'
        go to 700
     end select

     
     


     ! Containing internal subprograms
   contains
     function RTD(x)
       real :: RTD,x
       RTD = x*180/pi
     end function RTD

     function DTR(x)
       real:: DTR,x
       DTR = x*pi/180
     end function DTR

   end program main
