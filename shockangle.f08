  subroutine shockangle (mach,def,gam,strong,weak)
    implicit none
    real, intent(in) :: mach,def,gam
    real, intent(out) :: strong,weak
    real :: xnew,xold,A,B,C,error,tet1,tet2,x2
    integer :: i = 0
    
    A =  mach**2-1
    B = (gam+1)/2*mach**4*tan(def)
    C = (1+(gam+1)/2*mach**2)*tan(def)
    xold = sqrt(A)
    error = 1

    do while (error .gt. 0.0005)
       i = i + 1
       xnew = sqrt(A-B/(xold+C))
       error = abs(xnew-xold)/xold
       xold = xnew
       if (error .gt. 2) print*, 'The iteration for shock angle is diverging, please terminate the operation'
    end do

    print*, 'The shock angle solution is converged at', i, 'th iteration'
    
    x2 = 0.5*(-xnew-C + sqrt((xnew + C)*(C-3*xnew)+4*A))

    tet1 = atan(1/xnew)
    tet2 = atan(1/x2)

    if (tet1 .gt. tet2) then
       strong = tet1
       weak = tet2
    else
       strong = tet2
       weak = tet1
    endif
    



  end subroutine shockangle
